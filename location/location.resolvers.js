const LocationService = require('./location.service');

module.exports = {
  Query: {
    scan: (_, { listUid }, { userId }) => LocationService.scan(userId, listUid)
  },
  User: {
    id: u => u._id.toString()
  }
};
