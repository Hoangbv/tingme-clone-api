const graphqlHTTP = require('express-graphql');
const graphqlTools = require('graphql-tools');

const resolvers = require('./location.resolvers');
const typeDefs = require('./location.typeDefs');

const schema = graphqlTools.makeExecutableSchema({
  typeDefs,
  resolvers
});

module.exports = api => {
  api.use('/location', graphqlHTTP({ schema }));
};
