const mongoose = require('mongoose');

const User = require('../user/user.model');

const Request = require('../request/request.model');

const scan = async (userId, listUid) => {
  const nearUsers = await User.find({ uid: { $in: listUid } }).lean();

  const notRequestUsers = await Promise.all(
    nearUsers.map(async user => {
      const request = await Request.findOne({
        userId,
        targetId: user._id.toString(),
        status: 'pending'
      });

      if (request) {
        return null;
      }
      return user;
    })
  );

  const resUsers = notRequestUsers.filter(user => !user);

  return resUsers;
};

module.exports = {
  scan: (userId, listUid) => scan(userId, listUid)
};
