module.exports = /* GraphQL */ `
  type User {
    id: String
    avatar: String
    bioImages: [String]
    name: String
    phone: String
    dateOfBirth: String
    gender: String
    address: String
    views: Int
    scans: Int
  }

  type Query {
    scan(listUid: [String]): [User]
  }

  type Mutation {
    reportLocation(lat: Float, lon: Float): Boolean
  }

  schema {
    query: Query
    mutation: Mutation
  }
`;
