module.exports = /* GraphQL */ `
  type User {
    id: String
    avatar: String
    bioImages: [String]
    name: String
    phone: String
    dateOfBirth: String
    gender: String
    address: String
    views: Int
    scans: Int
  }

  type Query {
    getListRequest: [User]
    getListContact: [User]
  }

  type Mutation {
    requestUser(targetId: String): Boolean
    acceptRequest(requestId: String): Boolean
    declineRequest(requestId: String): Boolean
    cancelRequest(requestId: String): Boolean
  }

  schema {
    query: Query
    mutation: Mutation
  }
`;
