const mongoose = require('mongoose');

const requestSchema = new mongoose.Schema({
  userId: String,
  targetId: String,
  status: String
});

module.exports = mongoose.model('request', requestSchema);
