const RequestService = require('./request.service');

module.exports = {
  Query: {
    getListRequest: (_, __, { userId }) => RequestService.getListRequest(userId),
    getListContact: (_, __, { userId }) => RequestService.getListContact(userId)
  },
  Mutation: {
    requestUser: (_, { targetId }, { userId }) => RequestService.requestUser(targetId, userId),
    acceptRequest: (_, { requestId }, { userId }) =>
      RequestService.acceptRequest(requestId, userId),
    declineRequest: (_, { requestId }, { userId }) =>
      RequestService.declineRequest(requestId, userId),
    cancelRequest: (_, { requestId }, { userId }) => RequestService.cancelRequest(requestId, userId)
  },
  User: {
    id: u => u._id.toString()
  }
};
