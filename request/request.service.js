const User = require('../user/user.model');
const Request = require('./request.model');

const acceptRequest = async (requestId, targetId) => {
  const request = await Request.findOne({ requestId, targetId });
  if (!request) {
    return false;
  }
  request.status = 'accepted';

  await request.save();
  return true;
};

const declineRequest = async (requestId, targetId) => {
  const request = await Request.findOne({ requestId, targetId });
  if (!request) {
    return false;
  }
  request.status = 'declined';

  await request.save();
  return true;
};

const cancelRequest = async (requestId, userId) => {
  const request = await Request.findOne({ requestId, userId });
  if (!request) {
    return false;
  }
  request.status = 'canceled';

  await request.save();
  return true;
};

const getListRequest = async targetId => {
  const requests = await Request.find({ targetId, status: 'pending' })
    .select({ userId: 1, _id: 0 })
    .lean();
  const users = await Promise.all(requests.map(req => User.findById(req.userId).lean()));
  return users;
};

const getListContact = async userId => {
  const requests = await Request.find({
    $or: {
      userId,
      targetId: userId
    },
    status: 'accepted'
  })
    .select({ userId: 1, _id: 0 })
    .lean();
  const users = await Promise.all(requests.map(req => User.findById(req.userId).lean()));
  return users;
};

module.exports = {
  requestUser: (targetId, userId) => Request.create({ targetId, userId, status: 'pending' }),
  acceptRequest: (requestId, targetId) => acceptRequest(requestId, targetId),
  declineRequest: (requestId, targetId) => declineRequest(requestId, targetId),
  cancelRequest: (requestId, userId) => cancelRequest(requestId, userId),
  getListRequest: userId => getListRequest(userId),
  getListContact: userId => getListContact(userId)
};
