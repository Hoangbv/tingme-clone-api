const graphqlHTTP = require('express-graphql');
const graphqlTools = require('graphql-tools');

const resolvers = require('./request.resolvers');
const typeDefs = require('./request.typeDefs');

const schema = graphqlTools.makeExecutableSchema({
  typeDefs,
  resolvers
});

module.exports = api => {
  api.use('/request', graphqlHTTP({ schema }));
};
