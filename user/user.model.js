const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  avatar: String,
  bio: String,
  bioImages: [String],
  name: String,
  phone: String,
  dateOfBirth: Date,
  gender: String,
  address: String,
  views: Number,
  scans: Number,
  otp: String,
  location: [Number],
  uid: String
});

userSchema.index({
  location: '2d'
});

module.exports = mongoose.model('user', userSchema);
