const graphqlHTTP = require('express-graphql');
const graphqlTools = require('graphql-tools');
const multer = require('multer')({ dest: 'uploads/' });
const fs = require('fs');
const path = require('path');

const resolvers = require('./user.resolvers');
const typeDefs = require('./user.typeDefs');
const User = require('./user.model');
const { requiredUser } = require('./authen.middlewares');

const schema = graphqlTools.makeExecutableSchema({
  typeDefs,
  resolvers
});

module.exports = api => {
  api.use('/user', graphqlHTTP({ schema }));

  api.put('/avatar', requiredUser, multer.single('image'), (req, res, next) => {
    if (!fs.existsSync(`/uploads/${req.userId}/avatar`)) {
      fs.mkdirSync(`./uploads/${req.userId}/avatar`, { recursive: true });
    }
    fs.readFile(req.file.path, async (error, content) => {
      try {
        const user = await User.findById(req.userId);
        fs.unlink(user.avatar, () => {});
        const filePath = path.join(
          __dirname,
          `../uploads/${req.userId}/avatar`,
          req.file.originalname
        );
        fs.writeFile(filePath, content, err => {
          if (err) {
            throw err;
          }
        });
        user.avatar = filePath;
        await user.save();
        res.status(200);
        res.end();
      } catch (e) {
        next(e);
      } finally {
        fs.unlink(req.file.path, () => {});
      }
    });
  });

  api.put('/bio-images', requiredUser, multer.array('images'), async (req, res, next) => {
    if (!fs.existsSync(`/uploads/${req.userId}/bio-images`)) {
      fs.mkdirSync(`./uploads/${req.userId}/bio-images`, { recursive: true });
    }
    const user = await User.findById(req.userId);

    user.bioImages.forEach(file => {
      fs.unlink(file, () => {});
    });

    const bioImages = await Promise.all(
      req.files.map(async file => {
        const content = fs.readFileSync(file.path);
        try {
          const filePath = path.join(__dirname, `../uploads/${req.userId}`, file.originalname);
          fs.writeFile(filePath, content, err => {
            if (err) {
              throw err;
            }
          });
          return filePath;
        } catch (e) {
          next(e);
        } finally {
          fs.unlink(file.path, () => {});
        }
      })
    );

    user.bioImages = bioImages;
    await user.save();
    res.status(200);
    res.end();
  });
};
