const jwt = require('jsonwebtoken');
const moment = require('moment');

const User = require('./user.model');

const generateOTP = () => {
  let otp = '';
  const pattern = '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
  for (let index = 0; index < 8; index++) {
    otp += pattern[Math.floor(Math.random() * pattern.length)];
  }
  return otp;
};

const APP_SECRET = process.env.APP_SECRET;

const login = async (phone, otp, uid) => {
  const user = await User.findOne({ phone });
  user.uid = uid;
  await user.save();
  if (user) {
    return jwt.sign({ userId: user._id }, APP_SECRET);
  }
  throw new Error('Unauthorized!');
};

const requestOtp = async phone => {
  const user = await User.findOne({ phone });
  const otp = generateOTP();

  if (!user) {
    await User.create({ otp, phone });
    return true;
  }

  user.otp = otp;

  await user.save();
  return true;
};

const updateProfile = async (userId, name, bio, address, gender, dateOfBirth) => {
  const user = await User.findById(userId);

  console.log(userId);

  if (name) {
    user.name = name;
  }
  if (bio) {
    user.bio = bio;
  }
  if (address) {
    user.address = address;
  }
  if (gender) {
    user.gender = gender;
  }
  if (dateOfBirth) {
    user.dateOfBirth = moment(dateOfBirth, 'YYYY-MM-DD').startOf('day');
  }

  await user.save();

  return true;
};

module.exports = {
  login: (phone, otp, uid) => login(phone, otp, uid),
  requestOtp: phone => requestOtp(phone),
  updateProfile: (userId, name, bio, address, gender, dateOfBirth) =>
    updateProfile(userId, name, bio, address, gender, dateOfBirth),
  getUser: async (userId, self) => {
    const user = await User.findById(userId);
    if (!self) {
      user.views = user.views + 1;
    }
    await user.save();
    return user;
  }
};
