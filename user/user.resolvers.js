const UserService = require('./user.service');

module.exports = {
  Query: {
    requestOtp: (_, { phone }) => UserService.requestOtp(phone),
    login: (_, { phone, otp, uid }) => UserService.login(phone, otp, uid),
    getUserProfile: (_, { userId }) => UserService.getUser(userId, false),
    getSelfProfile: (_, __, { userId }) => UserService.getUser(userId, true)
  },
  Mutation: {
    updateProfile: (_, { name, bio, address, gender }, { userId }) =>
      UserService.updateProfile(userId, name, bio, address, gender, dateOfBirth)
  },
  User: {
    id: u => u._id.toString()
  }
};
