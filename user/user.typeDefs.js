module.exports = /* GraphQL */ `
  type User {
    id: String
    avatar: String
    bioImages: [String]
    name: String
    phone: String
    dateOfBirth: String
    gender: String
    address: String
    views: Int
    scans: Int
  }

  type Query {
    requestOtp(phone: String): Boolean
    login(phone: String, otp: String, uid: String): String
    getSelfProfile: User
    getUserProfile(userId: String): User
  }

  type Mutation {
    updateProfile(
      name: String
      bio: String
      address: String
      gender: String
      dateOfBirth: String
    ): Boolean
  }

  schema {
    query: Query
    mutation: Mutation
  }
`;
