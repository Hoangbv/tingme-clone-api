const User = require('./user.model');
const jwt = require('jsonwebtoken');

const APP_SECRET = process.env.APP_SECRET || 'sample';

module.exports = {
  checkUser: async (req, res, next) => {
    try {
      const token = req.header('x-access-token');
      if (!token) return next();
      const data = await jwt.verify(token, APP_SECRET);
      if (!data) return next();
      req.userId = data.userId;
      next();
    } catch (e) {
      next();
    }
  },
  requiredUser: async (req, res, next) => {
    const { userId } = req;
    if (!userId) {
      const error = new Error('ACCESS_DENY');
      return next(error);
    }
    const user = await User.findById(userId).lean();
    req.user = user;
    next();
  }
};
