require('dotenv').config();

const mongoose = require('mongoose');

const { checkUser } = require('./user/authen.middlewares');

mongoose.connect(
  process.env.MONGO_URL || 'mongodb://localhost:27017/tingmetest',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  },
  () => {
    console.log('connected');
  }
);

const express = require('express');
const cors = require('cors');
const createError = require('http-errors');

const app = express();
app.use(cors());

app.use(checkUser);

const api = express.Router();

app.get('/', (_, res) => res.send("It's working!"));
app.use('/api', api);
app.use('/test-error', (_, __, next) => next(createError(400, 'Intentional error!')));

require('./user/user.graphql')(api);
require('./request/request.graphql')(api);
require('./location/location.graphql')(api);

app.use((err, req, res, next) => {
  console.error(err);
  res.status(err.status || 500).send(err.message);
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log(`Server started at port ${PORT}`));
